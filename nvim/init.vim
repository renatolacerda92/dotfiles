" Useless under nvim  {{{

" " Autoindent works great with smarttab, makes deleting tabs easy
" set autoindent
" set smarttab

" " Autoload files that have changed outside of vim
" set autoread

" " Allow backspace to delete end of line, indent and start of line characters
" set backspace=indent,eol,start

" " UTF encoding
" set encoding=utf-8

" " Command history
" set history=100

" " Highlight search matches
" set hlsearch

" " Incremental searching (search as you type)
" set incsearch

" " Always show status bar
" set laststatus=2


" " Visual autocomplete for command menu (e.g. :e ~/path/to/file)
" set wildmenu


" }}}

" Settings {{{
" Switch syntax highlighting on, when the terminal has colors
" syntax on

" Use vim, not vi api
set nocompatible

" No backup files
set nobackup

" No write backup
set nowritebackup

" No swap file
set noswapfile

" Hide ruler when in Insert mode
:autocmd InsertEnter,InsertLeave * set cul!

" Show incomplete commands
set showcmd

let g:airline_powerline_fonts = 1

" Ignore case in search
set smartcase

" Make sure any searches /searchPhrase doesn't need the \c escape character
set ignorecase

" A buffer is marked as ‘hidden’ if it has unsaved changes, and it is not currently loaded in a window
" if you try and quit Vim while there are hidden buffers, you will raise an error:
" E162: No write since last change for buffer “a.txt"
set hidden

" Turn word wrap off
set nowrap


" Convert tabs to spaces
set expandtab


" Set tab size in spaces (this is for manual indenting)
set tabstop=4

" The number of spaces inserted for a tab (used for auto indenting)
set shiftwidth=4

" Turn on line numbers
set number

" Get rid of the delay when pressing O (for example)
" http://stackoverflow.com/questions/2158516/vim-delay-before-o-opens-a-new-line
set timeout timeoutlen=1000 ttimeoutlen=100


" Set the status line to something useful
set statusline=%f\ %=L:%l/%L\ %c\ (%p%%)

" Hide the toolbar
set guioptions-=T


" Use system clipboard
" http://stackoverflow.com/questions/8134647/copy-and-paste-in-vim-via-keyboard-between-different-mac-terminals
set clipboard+=unnamedplus

" Don't show intro
set shortmess+=I

" Better splits (new windows appear below and to the right)
set splitbelow
set splitright

" Highlight the current line
set cursorline

" redraw only when we need to (i.e. don't redraw when executing a macro)
set lazyredraw

" Clear search buffer
:nnoremap § :nohlsearch<cr>

call plug#begin('~/.local/share/nvim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-surround'
Plug 'equalsraf/neovim-gui-shim'
" Plug 'arcticicestudio/nord-vim'
Plug 'scrooloose/nerdtree'
Plug 'mhinz/vim-startify'
Plug 'liuchengxu/vim-which-key'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'jeetsukumaran/vim-buffergator'
call plug#end()

let mapleader = " "

let g:dracula_italic = 0
colorscheme dracula

nnoremap <silent> <leader> :WhichKey '<Space>'<CR>

map <C-n> :NERDTreeToggle<CR>:echo ""<CR>

let g:buffergator_suppress_keymaps = 1

" My custom fuctions
" Function to change gui font size on the fly
let g:fontsize = 11

function! AdjustFontSize(amount)
  let g:fontsize = g:fontsize+a:amount
  :execute "GuiFont! Hack:h" . g:fontsize
endfunction

function! SetFontSize()
  call inputsave()
  let size = input('Font size: ')
  call inputrestore()
  let g:fontsize = size
  :execute "GuiFont! Hack:h" . g:fontsize
endfunction

function! ToggleWrap()
  if &wrap
    echo "Wrap OFF"
    setlocal nowrap
    set virtualedit=all
    silent! nunmap <buffer> j
    silent! nunmap <buffer> k
    silent! nunmap <buffer> $
    silent! nunmap <buffer> 0
  else
    echo "Wrap ON"
    setlocal wrap linebreak nolist
    set virtualedit=
    setlocal display+=lastline
    noremap  <buffer> <silent> k gk
    noremap  <buffer> <silent> j gj
    noremap  <buffer> <silent> $ g$
    noremap  <buffer> <silent> 0 g0
  endif
endfunction


" My bindings for Spell and Style
nmap <leader>sw :call ToggleWrap()<CR>
nmap <leader>sp :set spell spelllang=pt_br<CR>
nmap <leader>s+ :call AdjustFontSize(1)<CR>
nmap <leader>s- :call AdjustFontSize(-1)<CR>
nmap <leader>sf :call SetFontSize()<CR>

" Bindings for tab control
nmap <leader>tt :tabnew<CR>
nmap <leader>tn :tabn<CR>
nmap <leader>tp :tabp<CR>
nmap <leader>tc :tabclose<CR>

" Bindings for loading abbreviations
iabbrev MA Mata Atlântica
iabbrev bf beija-flor
iabbrev bfs beija-flores
iabbrev bf beija-flor
iabbrev bro Bromeliaceae
iabbrev ap Aechmea pectinata

" Bindings for spellchecking (Z)
nmap <leader>zz z=
nmap <leader>zn ]s
nmap <leader>zp [s
nmap <leader>za zg
" 1z= takes the first suggestion
nmap <leader>zf 1z=

" Bindins for Buffer control
nmap <leader>bl :BuffergatorToggle<CR>:echo ""<CR>


" }}}
