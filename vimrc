" Settings {{{
" Switch syntax highlighting on, when the terminal has colors
syntax on

" Use vim, not vi api
set nocompatible

" No backup files
set nobackup

" No write backup
set nowritebackup

" No swap file
set noswapfile

" Command history
set history=100

" Hide ruler when in Insert mode
:autocmd InsertEnter,InsertLeave * set cul!

" Show incomplete commands
set showcmd

" Incremental searching (search as you type)
set incsearch

let g:airline_powerline_fonts = 1

" Highlight search matches
set hlsearch

" Ignore case in search
set smartcase

" Make sure any searches /searchPhrase doesn't need the \c escape character
set ignorecase

" A buffer is marked as ‘hidden’ if it has unsaved changes, and it is not currently loaded in a window
" if you try and quit Vim while there are hidden buffers, you will raise an error:
" E162: No write since last change for buffer “a.txt"
set hidden

" Turn word wrap off
set nowrap

" Allow backspace to delete end of line, indent and start of line characters
set backspace=indent,eol,start

" Convert tabs to spaces
set expandtab

" Autoindent works great with smarttab, makes deleting tabs easy
set autoindent
set smarttab

" Set tab size in spaces (this is for manual indenting)
set tabstop=4

" The number of spaces inserted for a tab (used for auto indenting)
set shiftwidth=4

" Turn on line numbers
set number

" Get rid of the delay when pressing O (for example)
" http://stackoverflow.com/questions/2158516/vim-delay-before-o-opens-a-new-line
set timeout timeoutlen=1000 ttimeoutlen=100

" Always show status bar
set laststatus=2

" Set the status line to something useful
set statusline=%f\ %=L:%l/%L\ %c\ (%p%%)

" Hide the toolbar
set guioptions-=T

" UTF encoding
set encoding=utf-8

" Autoload files that have changed outside of vim
set autoread

" Use system clipboard
" http://stackoverflow.com/questions/8134647/copy-and-paste-in-vim-via-keyboard-between-different-mac-terminals
set clipboard+=unnamed

" Don't show intro
set shortmess+=I

" Better splits (new windows appear below and to the right)
set splitbelow
set splitright

" Highlight the current line
set cursorline

" Visual autocomplete for command menu (e.g. :e ~/path/to/file)
set wildmenu

" redraw only when we need to (i.e. don't redraw when executing a macro)
set lazyredraw

" highlight a matching [{()}] when cursor is placed on start/end character
set showmatch

" Set built-in file system explorer to use layout similar to the NERDTree plugin
let g:netrw_liststyle=3

" Clear search buffer
:nnoremap § :nohlsearch<cr>

call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-surround'
call plug#end()

let mapleader = " "


" Always highlight column 80 so it's easier to see where
" cutoff appears on longer screens
" autocmd BufWinEnter * highlight ColorColumn ctermbg=darkred
" set colorcolumn=80
" }}}

" Command to use sudo when needed
" cmap w!! %!sudo tee > /dev/null %

" File System Explorer (in horizontal split)
" map <leader>. :Sexplore<cr>

" Buffers
" map <leader>yt :ls<cr>

" Buffers (runs the delete buffer command on all open buffers)
" map <leader>yd :bufdo bd<cr>

" Make handling vertical/linear Vim windows easier
" map <leader>w- <C-W>- " decrement height
" map <leader>w+ <C-W>+ " increment height
" map <leader>w] <C-W>_ " maximize height
" map <leader>w[ <C-W>= " equalize all windows

" Handling horizontal Vim windows doesn't appear to be possible.
" Attempting to map <C-W> < and > didn't work
" Same with mapping <C-W>|

" Make splitting Vim windows easier
" map <leader>; <C-W>s
" map <leader>` <C-W>v

" specify syntax highlighting for specific files
" autocmd Bufread,BufNewFile *.md set filetype=markdown " Vim interprets .md as 'modula2' otherwise, see :set filetype?

" Create a 'scratch buffer' which is a temporary buffer Vim wont ask to save
" http://vim.wikia.com/wiki/Display_output_of_shell_commands_in_new_window
" command! -complete=shellcmd -nargs=+ Shell call s:RunShellCommand(<q-args>)
" function! s:RunShellCommand(cmdline)
"   echo a:cmdline
"   let expanded_cmdline = a:cmdline
"   for part in split(a:cmdline, ' ')
"     if part[0] =~ '\v[%#<]'
"       let expanded_part = fnameescape(expand(part))
"       let expanded_cmdline = substitute(expanded_cmdline, part, expanded_part, '')
"     endif
"   endfor
"   botright new
"   setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap
"   call setline(1, 'You entered:    ' . a:cmdline)
"   call setline(2, 'Expanded Form:  ' .expanded_cmdline)
"   call setline(3,substitute(getline(2),'.','=','g'))
"   execute '$read !'. expanded_cmdline
"   setlocal nomodifiable
"   1
" endfunction

" Rainbow parenthesis always on!
" if exists(':RainbowParenthesesToggle')
"   autocmd VimEnter * RainbowParenthesesToggle
"   autocmd Syntax * RainbowParenthesesLoadRound
"   autocmd Syntax * RainbowParenthesesLoadSquare
"   autocmd Syntax * RainbowParenthesesLoadBraces
" endif

" Reset spelling colours when reading a new buffer
" This works around an issue where the colorscheme is changed by .local.vimrc
" fun! SetSpellingColors()
"   highlight SpellBad cterm=bold ctermfg=white ctermbg=red
"   highlight SpellCap cterm=bold ctermfg=red ctermbg=white
" endfun
" autocmd BufWinEnter * call SetSpellingColors()
" autocmd BufNewFile * call SetSpellingColors()
" autocmd BufRead * call SetSpellingColors()
" autocmd InsertEnter * call SetSpellingColors()
" autocmd InsertLeave * call SetSpellingColors()

