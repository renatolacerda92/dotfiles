import re
import urllib.request
import zipfile
import os
import argparse

def find_images(source):
    url_list = []
    
    with open(source) as f:
        for line in f:
            match = re.search('lstImages\.push', line) # lstImages is one way to find links
            if match is not None:
                url = re.search('\"http.*\"', line)
                # group() return the matching regex, since it also looks for double quotes, we remove them with strip
                url_list.append((url.group().strip('\"')))
    
    print("Found {!s} pages".format(len(url_list)))
    return url_list

def download_files(url_list, name_list):
    for i, url in enumerate(url_list):
        print("Downloading Page {!s} out of {!s}".format(i, len(url_list)), end="\r")
        urllib.request.urlretrieve(url, filenames[i])

def save_to_cbz(name):
    print("\nCreating {} file".format(name))
    name_cbz = name + ".cbz"

    with zipfile.ZipFile(name_cbz, 'w') as myzip:
        for file in filenames:
            myzip.write(file)
            os.remove(file)
            


parser = argparse.ArgumentParser()
parser.add_argument("-r", "--remove", help="remove html files after download is done", action="store_true")
parser.add_argument("files", nargs='+')
args = parser.parse_args()

for file in args.files:
    file_name = file.split(".")[0]
    print("Starting dowload of {}".format(file_name))
    url_list = find_images(file)
    filenames = list(file_name + "_page" + str(n) + ".jpg" for n in range(len(url_list)))
    download_files(url_list, filenames)
    save_to_cbz(file_name)
    print("{} is done\n".format(file))

if args.remove:
    print("Deleting the html files")
    for file in args.files:
        os.remove(file)
